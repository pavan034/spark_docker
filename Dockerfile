FROM centos:7
RUN yum -y update
RUN yum -y install git net-tools nano wget
RUN yum -y install openssh-server
RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
  systemd-tmpfiles-setup.service ] || rm -f $i; done); \
  rm -f /lib/systemd/system/multi-user.target.wants/*;\
  rm -f /etc/systemd/system/*.wants/*;\
  rm -f /lib/systemd/system/local-fs.target.wants/*; \
  rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
  rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
  rm -f /lib/systemd/system/basic.target.wants/*;\
  rm -f /lib/systemd/system/anaconda.target.wants/*;
VOLUME [ "/sys/fs/cgroup" ]
RUN systemctl enable sshd.service
RUN yum -y install telnet
RUN yum install -y java-1.8.0-openjdk
RUN yum install -y java-1.8.0-openjdk-devel
RUN yum install -y sudo
COPY pass /root
RUN adduser admin
RUN cat /root/pass | passwd admin
RUN usermod -aG wheel admin
RUN /usr/bin/ssh-keygen -A
RUN rm -rf /run/nologin
USER admin
RUN mkdir -p /home/admin/cluster
RUN cd /home/admin/cluster && wget https://archive.apache.org/dist/hadoop/common/hadoop-3.2.0/hadoop-3.2.0.tar.gz
RUN cd /home/admin/cluster && tar -zxvf hadoop-3.2.0.tar.gz && mv hadoop-3.2.0 hadoop && rm hadoop-3.2.0.tar.gz
RUN cd /home/admin/cluster/hadoop && rm -R etc
RUN cd /home/admin/cluster/ && wget http://mirrors.estointernet.in/apache/zookeeper/zookeeper-3.4.14/zookeeper-3.4.14.tar.gz && tar -zxvf zookeeper-3.4.14.tar.gz && rm zookeeper-3.4.14.tar.gz
RUN cd /home/admin/cluster/ && mv zookeeper-3.4.14 zookeeper
RUN mkdir -p /home/admin/cluster/zookeeper/data
RUN mkdir -p /home/admin/cluster/zookeeper/datalogs
RUN touch /home/admin/cluster/zookeeper/data/myid
RUN echo "1" > /home/admin/cluster/zookeeper/data/myid
USER root
RUN sudo mkdir -p /opt/hadoop/data/hdfs/namenode
RUN sudo mkdir -p /opt/hadoop/data/hdfs/datanode
RUN chown admin:admin -R /opt/hadoop
USER admin
RUN cd /home/admin/cluster/ && wget https://archive.apache.org/dist/spark/spark-2.3.3/spark-2.3.3-bin-hadoop2.7.tgz && tar -zxvf spark-2.3.3-bin-hadoop2.7.tgz && mv spark-2.3.3-bin-hadoop2.7 spark && rm spark-2.3.3-bin-hadoop2.7.tgz
RUN cd /home/admin/cluster/ && wget https://archive.apache.org/dist/oozie/4.3.1/oozie-4.3.1.tar.gz && tar -zxvf oozie-4.3.1.tar.gz && mv oozie-4.3.1 oozie && rm oozie-4.3.1.tar.gz
USER root
RUN yum install -y gcc
RUN yum install -y https://centos7.iuscommunity.org/ius-release.rpm
RUN yum install -y python36u python36u-libs python36u-devel python36u-pip
RUN yum install -y which maven
RUN echo "admin    ALL=(ALL)	  NOPASSWD:ALL" >> /etc/sudoers
RUN yum -y install python3-psycopg2
RUN yum -y install haproxy
COPY requirements.txt /root
RUN pip3 install -r /root/requirements.txt
USER admin
RUN mkdir /home/admin/.ssh
RUN ssh-keygen -t rsa -f /home/admin/.ssh/id_rsa -P ""
RUN cp /home/admin/.ssh/id_rsa.pub /home/admin/.ssh/authorized_keys
RUN ssh-keyscan -H localhost >> /home/admin/.ssh/known_hosts
RUN printf "Host *\n StrictHostKeyChecking no">/home/admin/.ssh/config
RUN chown -R admin:admin /home/admin/.ssh
RUN chmod 400 /home/admin/.ssh/config
COPY etc /home/admin/cluster/hadoop/etc
ENV HADOOP_HOME /home/admin/cluster/hadoop
ENV HADOOP_INSTALL /home/admin/cluster/hadoop
ENV HADOOP_MAPRED_HOME /home/admin/cluster/hadoop
ENV HADOOP_COMMON_HOME /home/admin/cluster/hadoop
ENV HADOOP_HDFS_HOME /home/admin/cluster/hadoop
ENV HADOOP_YARN_HOME /home/admin/cluster/hadoop
ENV HADOOP_COMMON_LIB_NATIVE_DIR /home/admin/cluster/hadoop/lib/native
ENV HADOOP_CONF_DIR /home/admin/cluster/hadoop/etc/hadoop
ENV SPARK_HOME /home/admin/cluster/spark
ENV ZOOKEEPER_HOME /home/admin/cluster/zookeeper
ENV PYSPARK_PYTHON python3
ENV FLASK_APP /home/admin/cluster/flask_app
RUN mkdir /home/admin/model
COPY app.py /home/admin/model/
COPY wsgi.py /home/admin/
COPY run.sh /home/admin/
COPY conf.py /home/admin/
COPY spark-defaults.conf /home/admin/cluster/spark/conf/
COPY haproxy.cfg /etc/haproxy/haproxy.cfg
COPY zoo.cfg /home/admin/cluster/zookeeper/conf/
USER root
RUN chmod 777 -R /home/admin/cluster/zookeeper/conf
RUN chmod 777 -R /home/admin/cluster/zookeeper/data/myid
RUN chmod 777 -R /home/admin/cluster/spark/conf/spark-defaults.conf
RUN chmod 777 -R /home/admin/cluster/hadoop/etc
COPY oozie-bin /home/admin/cluster/oozie/oozie-bin
RUN chmod 777 -R /home/admin/cluster/oozie/oozie-bin
RUN chmod 777 -R /etc/haproxy/haproxy.cfg
USER admin
ENV LANG en_US.utf-8
ENV OOZIE_HOME /home/admin/cluster/oozie/oozie-bin
ENV PATH="/home/admin/cluster/hadoop/sbin/:${PATH}"
ENV PATH="/home/admin/cluster/hadoop/bin/:${PATH}"
ENV PATH="/home/admin/cluster/spark/bin/:${PATH}"
ENV PATH="/home/admin/cluster/zookeeper/bin:${PATH}"
ENV PATH="/home/admin/cluster/oozie/oozie-bin/bin:${PATH}"
COPY postgresql-42.2.5.jar /home/admin/cluster/spark/jars/
COPY spark-cassandra-connector_2.11-2.3.3.jar /home/admin/cluster/spark/jars/
COPY jsr166e-1.1.0.jar /home/admin/cluster/spark/jars/
COPY mssql-jdbc-6.1.0.jre8.jar /home/admin/cluster/spark/jars/
COPY sshd_config /etc/ssh/sshd_config
WORKDIR /home/admin
