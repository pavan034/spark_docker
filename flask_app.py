from flask import Flask, url_for
from flask import request
from flask import json
import os,subprocess
#import acqueon_new as ac
#import subprocess as sb
app = Flask(__name__)
#app.run(host='0.0.0.0' , port=5000)

@app.route('/')
def api_root():
    return 'Welcome'


@app.route('/execute', methods=['POST'])
def execute():
    if request.headers['Content-Type'] == 'application/json':
        jsonvalue = json.dumps(request.json)
        input_obj = json.loads(jsonvalue)
        table_test = str(input_obj['table_test'])
        url = str(input_obj['url'])
        userid = str(input_obj['userid'])
        password = str(input_obj['password'])
        table_processed = 'onelac_p2p_processed_updated' #'aiq_p2p_processed'
        keyspace = str(input_obj['keyspace'])
        modelversionID = str(input_obj['modelversionID'])
        jobid = str(input_obj['jobid'])
        tenant = str(input_obj['tenant'])
        week1=str(input_obj['week1'])
        week2=str(input_obj['week2'])
        print(input_obj)
        print(table_test,url,userid,password,table_processed,keyspace,modelversionID,jobid)
        cmd = 'spark-submit --master yarn --deploy-mode cluster /home/admin/cluster/p2p_prediction.py'+' '+ table_test +' '+  url +' '+ userid +' '+  password+' '+  table_processed+' '+  keyspace+' '+  modelversionID+' '+  jobid +' '+ tenant +' ' +week1+' '+week2 +' &'
        print(cmd)
        os.system(cmd)  # returns the exit code in unix

#    url = "jdbc:postgresql://172.20.6.176:5432/aiq_sysadmin"
#    userid = "aiq_sysadmin"
#    password = "aiq_sysadmin"
#    table_processed = 'p2p_processeddata'
#    keyspace = 'aiq0001'
#    modelversionID = 'p2p'
#    jobid = 'job0016'    
#    name=ac.predfunction(table_test,url,userid,password,table_processed,keyspace,modelversionID,jobid)
#    acqueon_bttc.py
        return 'success'
 




@app.route('/execute_bttc', methods=['POST'])
def execute_bttc():
    if request.headers['Content-Type'] == 'application/json':
        jsonvalue = json.dumps(request.json)
        input_obj = json.loads(jsonvalue)
        table_test = str(input_obj['table_test'])
        url = str(input_obj['url'])
        userid = str(input_obj['userid'])
        password = str(input_obj['password'])
        table_processed = 'onelac_bttc_processed_updated'
        keyspace = str(input_obj['keyspace'])
        modelversionID = str(input_obj['modelversionID'])
        jobid = str(input_obj['jobid'])
        tenant = str(input_obj['tenant'])
        starttime=str(input_obj['starttime'])
        endtime = str(input_obj['endtime'])
        print(input_obj)
        print(table_test,url,userid,password,table_processed,keyspace,modelversionID,jobid,tenant,starttime,endtime)
        cmd = 'spark-submit --master yarn --deploy-mode cluster /home/admin/cluster/bttc_prediction.py'+' '+ table_test +' '+  url +' '+ userid +' '+  password+' '+  table_processed+' '+  keyspace+' '+  modelversionID+' '+  jobid +' '+ tenant +' '+'"'+starttime+'"'+' '+'"'+endtime+'"' +' &'
        print(cmd)
        #cmd1 ='hadoop fs -get /home/admin/cluster/spark/AIQ_BTTC_'+jobid+'_Results.csv/*.csv data/'+tenant+'/'+'AIQ_BTTC_'+jobid+'_Results.csv'
        os.system(cmd)  # returns the exit code in unix
        #os.system(cmd1)
        #    acqueon_bttc.py
        return 'success'




@app.route('/writecsv', methods=['POST'])
def execute_bttc_write():
    if request.headers['Content-Type'] == 'application/json':
        jsonvalue = json.dumps(request.json)
        input_obj = json.loads(jsonvalue)
        #table_test = str(input_obj['table_test'])
        #url = str(input_obj['url'])
        #userid = str(input_obj['userid'])
        #password = str(input_obj['password'])
        #table_processed = 'onelac_bttc_processed_updated'
        #keyspace = str(input_obj['keyspace'])
        #modelversionID = str(input_obj['modelversionID'])
        jobid = str(input_obj['jobid'])
        tenant = str(input_obj['tenant'])
        #starttime=str(input_obj['starttime'])
        #endtime = str(input_obj['endtime'])
        #print(input_obj)
        #print(table_test,url,userid,password,table_processed,keyspace,modelversionID,jobid,tenant,starttime,endtime)
        cmd = 'hadoop fs -rm -r /home/admin/cluster/spark/'+tenant+'/AIQ_BTTC_'+jobid+'_Results.csv/'
        cmd1 ='hadoop fs -get /home/admin/cluster/spark/'+tenant+'/AIQ_BTTC_'+jobid+'_Results.csv/*.csv /home/admin/cluster/data/'+tenant+'/'+'AIQ_BTTC_'+jobid+'_Results.csv'
       # print(cmd)
        #os.system(cmd)  # returns the exit code in unix
        os.system(cmd1)
        os.system(cmd)
        #    acqueon_bttc.py
        return 'success'





@app.route('/execute_feedback', methods=['POST'])
def execute_feedb():
    if request.headers['Content-Type'] == 'application/json':
        jsonvalue = json.dumps(request.json)
        input_obj = json.loads(jsonvalue)

        url = str(input_obj['url'])
        userid = str(input_obj['userid'])
        password = str(input_obj['password'])
        feedbackid = str(input_obj['feedbackid'])
        tenant = str(input_obj['tenant'])
        keyspace = str(input_obj['keyspace'])
        print(input_obj)
        print(url,userid,password,feedbackid,tenant)
        cmd = 'spark-submit --master yarn --deploy-mode cluster /home/admin/cluster/feedback_bttc.py'+' '+url+' '+userid+' '+password+' '+tenant+' '+feedbackid+' '+keyspace+' &'
        print(cmd)
        os.system(cmd)  # returns the exit code in unix
        #    acqueon_bttc.py
        return 'success'


@app.route('/execute_p2p', methods=['GET'])
def p2p():
    table_test = "\"AIQ_jobid001_Data\""
    url = "jdbc:postgresql://172.20.6.176:5432/aiq_sysadmin"
    userid = "aiq_sysadmin"
    password = "aiq_sysadmin"
    table_processed = 'p2p_processeddata'
    keyspace = 'aiq0001'
    modelversionID = 'p2p'
    jobid = 'job00125'
    #name=ac.predfunction(table_test,url,userid,password,table_processed,keyspace,modelversionID,jobid)
    cmd = 'spark-submit --master yarn --deploy-mode cluster /home/admin/cluster/acqueon07.py'+' '+ table_test +' '+  url +' '+ userid +' '+  password+' '+  table_processed+' '+  keyspace+' '+  modelversionID+' '+  jobid 
    print(cmd)
    os.system(cmd)  # returns the exit code in unix    
    return 'success'



@app.route('/killApp', methods=['GET'])
def killapp():
    appId = request.args.get('appId')
    cmd = 'yarn application -kill '+appId
#    appId = request.args.get('appId')
    os.system(cmd)  # returns the exit code in unix
    return 'success'



