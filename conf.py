import xml.etree.ElementTree as ET
import os
try:
    node1 = os.environ['FQDN1']
    node2 = os.environ['FQDN2']
    node3 = os.environ['FQDN3']
    myid = os.environ['MYID']
    zookeeper = os.environ['ZOOKEEPER_HOME']
    hadoop = os.environ['HADOOP_HOME']
    oozie = os.environ['OOZIE_HOME']
    #postgres = os.environ['POSTGRES_IP']
    #postgres_port = os.environ['POSTGRES_PORT']
    sqlserver= os.environ['SQLSERVER_HOST_NAME']
except Exception as e:
    print("Error from Environment variable--->",str(e))

def change_xml(path):
  with open(path) as f:
    tree = ET.parse(f)
    root = tree.getroot()
    for elem in root.getiterator():
      try:
        elem.text = elem.text.replace('node1', node1)
        elem.text = elem.text.replace('node2', node2)
        elem.text = elem.text.replace('node3', node3)
      except AttributeError:
        pass
  tree.write(path)

def change_txt_masters(path):
  file1 = open(path,"w") 
  file1.write(node1+ "\n"+node2)
  file1.close()

def change_slaves(path):
  file1 = open(path,"w")
  file1.write(node1+ "\n"+node2+"\n"+node3)
  file1.close()
import string
def change_zoo(path):
  s = open(path).read()
  s = s.replace('node1', node1)
  s = s.replace('node2', node2)
  s = s.replace('node3', node3)
  f = open(path, 'w')
  f.write(s)
  f.close()
def change_myid(path):
  file1 = open(path,"w")
  file1.write(myid)
  file1.close()
def oozie_conf(path):
  with open(path) as f:
    tree = ET.parse(f)
    root = tree.getroot()
    for elem in root.getiterator():
      try:
        elem.text = elem.text.replace('SQLSERVER_HOST_NAME',sqlserver)
        elem.text = elem.text.replace('node1', node1)
        elem.text = elem.text.replace('node2', node2)
        elem.text = elem.text.replace('node3', node3)
      except AttributeError:
        pass
  tree.write(path)
def change_haproxy(path):
  s = open(path).read()
  s = s.replace('node1', node1)
  s = s.replace('node2', node2)
  s = s.replace('node3', node3)
  f = open(path, 'w')
  f.write(s)
  f.close()

change_xml(hadoop+'/etc/hadoop/hdfs-site.xml')
change_xml(hadoop+'/etc/hadoop/core-site.xml')
change_xml(hadoop+'/etc/hadoop/yarn-site.xml')
change_xml(hadoop+'/etc/hadoop/mapred-site.xml')
change_txt_masters(hadoop+'/etc/hadoop/masters')
change_slaves(hadoop+'/etc/hadoop/slaves')
change_slaves(hadoop+'/etc/hadoop/workers')
change_zoo(zookeeper+'/conf/zoo.cfg')
change_myid(zookeeper+'/data/myid')
oozie_conf(oozie+'/conf/oozie-site.xml')
change_haproxy('/etc/haproxy/haproxy.cfg')
