from flask import Flask, url_for
from flask import request
from flask import json
import os,subprocess
#import acqueon_new as ac
#import subprocess as sb
app = Flask(__name__)
#app.run(host='0.0.0.0' , port=5000)

@app.route('/')
def api_root():
#    print(get_spark_conf())
    conf=get_spark_conf()
    cmd = 'spark-submit --master yarn --deploy-mode cluster '+conf+' /home/admin/model/p2p_prediction.py' 
    print(cmd)
    return 'Welcome'


def get_spark_conf():
    with open ("./model/spark.conf", "r") as myfile:
        data=myfile.readlines()
    
    spark_executor_instances=data[0].strip()
    spark_driver_memory=data[1].strip()
    spark_executor_memory=data[2].strip()
    spark_driver_cores=data[3].strip()
    spark_executor_cores=data[4].strip()
    spark_cassandra_connection_host=data[5].strip()
    
    spark_executor_instances=spark_executor_instances.split("=")
    spark_driver_memory=spark_driver_memory.split("=")
    spark_executor_memory=spark_executor_memory.split("=")
    spark_driver_cores=spark_driver_cores.split("=")
    spark_executor_cores=spark_executor_cores.split("=")
    spark_cassandra_connection_host=spark_cassandra_connection_host.split("=")
    #cmd = 'spark-submit --master yarn --deploy-mode cluster /home/admin/model/p2p_prediction.py ' #+' '+ table_test +' '+  url +' '+ userid +' '+  password+' '+  table_processed+' '+  keyspace+' '+  modelversionID+' '+  jobid +' '+ tenant +' ' +week1+' '+week2 +' &'
    #print(cmd)
    result=' --num-executors='+str(spark_executor_instances[1])+' --driver-memory='+str(spark_driver_memory[1])+' --executor-memory='+str(spark_executor_memory[1])+' --driver-cores='+str(spark_driver_cores[1])+' --executor-cores='+str(spark_executor_cores[1])+' --conf spark.cassandra.connection.host='+str(spark_cassandra_connection_host[1])
    return result


@app.route('/execute', methods=['POST'])
def execute():
    if request.headers['Content-Type'] == 'application/json':
        jsonvalue = json.dumps(request.json)
        input_obj = json.loads(jsonvalue)
        table_test = str(input_obj['table_test'])
        url = str(input_obj['url'])
        userid = str(input_obj['userid'])
        password = str(input_obj['password'])
        table_processed = 'onelac_p2p_processed_updated' #'aiq_p2p_processed'
        keyspace = str(input_obj['keyspace'])
        modelversionID = str(input_obj['modelversionID'])
        jobid = str(input_obj['jobid'])
        tenant = str(input_obj['tenant'])
        week1=str(input_obj['week1'])
        week2=str(input_obj['week2'])
        print(input_obj)
        conf=get_spark_conf()
        print(table_test,url,userid,password,table_processed,keyspace,modelversionID,jobid)
        cmd = 'spark-submit --master yarn --deploy-mode cluster '+conf+' /home/admin/model/p2p_prediction.py'+' '+ table_test +' '+  url +' '+ userid +' '+  password+' '+  table_processed+' '+  keyspace+' '+  modelversionID+' '+  jobid +' '+ tenant +' ' +week1+' '+week2 +' &'
        print(cmd)
        os.system(cmd)  # returns the exit code in unix

#    url = "jdbc:postgresql://172.20.6.176:5432/aiq_sysadmin"
#    userid = "aiq_sysadmin"
#    password = "aiq_sysadmin"
#    table_processed = 'p2p_processeddata'
#    keyspace = 'aiq0001'
#    modelversionID = 'p2p'
#    jobid = 'job0016'    
#    name=ac.predfunction(table_test,url,userid,password,table_processed,keyspace,modelversionID,jobid)
#    acqueon_bttc.py
        return 'success'
 




@app.route('/execute_bttc', methods=['POST'])
def execute_bttc():
    if request.headers['Content-Type'] == 'application/json':
        jsonvalue = json.dumps(request.json)
        input_obj = json.loads(jsonvalue)
        table_test = str(input_obj['table_test'])
        url = str(input_obj['url'])
        userid = str(input_obj['userid'])
        password = str(input_obj['password'])
        table_processed = 'onelac_bttc_processed_updated'
        keyspace = str(input_obj['keyspace'])
        modelversionID = str(input_obj['modelversionID'])
        jobid = str(input_obj['jobid'])
        tenant = str(input_obj['tenant'])
        starttime=str(input_obj['starttime'])
        endtime = str(input_obj['endtime'])
        print(input_obj)
        print(table_test,url,userid,password,table_processed,keyspace,modelversionID,jobid,tenant,starttime,endtime)
        conf=get_spark_conf()
        cmd = 'spark-submit --master yarn --deploy-mode cluster '+conf+' /home/admin/model/bttc_prediction.py'+' '+ table_test +' '+  url +' '+ userid +' '+  password+' '+  table_processed+' '+  keyspace+' '+  modelversionID+' '+  jobid +' '+ tenant +' '+'"'+starttime+'"'+' '+'"'+endtime+'"' +' &'
        print(cmd)
        #cmd1 ='hadoop fs -get /home/admin/cluster/spark/AIQ_BTTC_'+jobid+'_Results.csv/*.csv data/'+tenant+'/'+'AIQ_BTTC_'+jobid+'_Results.csv'
        os.system(cmd)  # returns the exit code in unix
        #os.system(cmd1)
        #    acqueon_bttc.py
        return 'success'




@app.route('/writecsv', methods=['POST'])
def execute_bttc_write():
    if request.headers['Content-Type'] == 'application/json':
        jsonvalue = json.dumps(request.json)
        input_obj = json.loads(jsonvalue)
        #table_test = str(input_obj['table_test'])
        #url = str(input_obj['url'])
        #userid = str(input_obj['userid'])
        #password = str(input_obj['password'])
        #table_processed = 'onelac_bttc_processed_updated'
        #keyspace = str(input_obj['keyspace'])
        #modelversionID = str(input_obj['modelversionID'])
        jobid = str(input_obj['jobid'])
        tenant = str(input_obj['tenant'])
        #starttime=str(input_obj['starttime'])
        #endtime = str(input_obj['endtime'])
        #print(input_obj)
        #print(table_test,url,userid,password,table_processed,keyspace,modelversionID,jobid,tenant,starttime,endtime)
        cmd = 'hadoop fs -rm -r /home/admin/model/'+tenant+'/AIQ_BTTC_'+jobid+'_Results.csv/'
        cmd1 ='hadoop fs -get /home/admin/model/'+tenant+'/AIQ_BTTC_'+jobid+'_Results.csv/*.csv /home/admin/model/data/'+tenant+'/'+'AIQ_BTTC_'+jobid+'_Results.csv'
       # print(cmd)
        #os.system(cmd)  # returns the exit code in unix
        os.system(cmd1)
        os.system(cmd)
        return 'success'


@app.route('/execute_feedback', methods=['POST'])
def execute_feedb():
    if request.headers['Content-Type'] == 'application/json':
        jsonvalue = json.dumps(request.json)
        input_obj = json.loads(jsonvalue)

        url = str(input_obj['url'])
        userid = str(input_obj['userid'])
        password = str(input_obj['password'])
        feedbackid = str(input_obj['feedbackid'])
        tenant = str(input_obj['tenant'])
        keyspace = str(input_obj['keyspace'])
        print(input_obj)
        print(url,userid,password,feedbackid,tenant)
        conf=get_spark_conf()
        cmd = 'spark-submit --master yarn --deploy-mode cluster '+conf+' /home/admin/model/feedback_bttc.py'+' '+url+' '+userid+' '+password+' '+tenant+' '+feedbackid+' '+keyspace+' &'
        print(cmd)
        os.system(cmd)  # returns the exit code in unix
        #    acqueon_bttc.py
        return 'success'



@app.route('/killApp', methods=['GET'])
def killapp():
    appId = request.args.get('appId')
    cmd = 'yarn application -kill '+appId
#    appId = request.args.get('appId')
    os.system(cmd)  # returns the exit code in unix
    return 'success'



